import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {

  screen=""

  constructor() { }
  


  ngOnInit(): void {
  }

  display(num:any){
    this.screen+=num
  }

  calculate(num:any){

    this.screen=eval(this.screen)

  }

  Clear(){
    this.screen=""
  }

  delete1(){
    this.screen=this.screen.slice(0,-1)
  }
  
}
